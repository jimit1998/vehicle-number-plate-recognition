package com.vehicle.number.plate.recognition.api;

import android.app.Application;
import android.util.Log;

public class GlobalClass extends Application {
    private static final String TAG = "GlobalClass";
    public static final String BASE_URL = "https://api.platerecognizer.com/";
    public static GlobalClass singleton;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Response: onCreate: url sent");
        singleton=this;
    }

    public static GlobalClass getInstance() {
        return singleton;
    }
}