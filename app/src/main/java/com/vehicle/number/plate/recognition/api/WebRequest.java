package com.vehicle.number.plate.recognition.api;

import android.content.Context;
import android.util.Log;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

public class WebRequest {
    private static final String TAG = "WebRequest";
    public static AsyncHttpClient client;

    static {
        client = new AsyncHttpClient(true, 80, 443);
    }

    public static void post(Context context, String url, RequestParams params,
                            ResponseHandlerInterface responseHandler) {
        client.post(context, url, params, responseHandler);
        Log.d(TAG, "Response: post: request sent");
    }
}